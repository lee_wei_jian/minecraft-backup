#!/bin/bash

BACKUP_BASE_PATH="/opt/minecraft-backup"

rclone sync /opt/minecraft-backup/hourly gdrive:/minecraft-backups/hourly -P --delete-before --drive-use-trash=false
rclone sync /opt/minecraft-backup/daily gdrive:/minecraft-backups/daily -P --delete-before --drive-use-trash=false
rclone sync /opt/minecraft-backup/weekly gdrive:/minecraft-backups/weekly -P --delete-before --drive-use-trash=false
rclone sync /opt/minecraft-backup/monthly gdrive:/minecraft-backups/monthly -P --delete-before --drive-use-trash=false
